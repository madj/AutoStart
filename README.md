# Auto Start As Windows Service

#### 介绍
很简单程序，就是使用Windows服务实现在服务器开机或者重启时自动启动cmd或者bat命令。
自己在配置代理服务器proxy时、在配置虚拟化软件VirtualBox时，经常需要把一些批处理文件设置成开机启动，那种开机后不需要登录Windows系统就能在后台自动启动。在网上也找过类似的软件，但都要求讲bat文件转化成exe文件，然后再通过第三方工具安装成Windows服务。整个过程比较复杂而且不方便再次修改调整bat文件的内容。为了方便自己使用，所以萌发了编写这个程序的念头，于是经过很长一段时间的考虑和一个晚上的努力就做出了这个程序。现在拿出来分享给大家，希望大家喜欢也多提建议。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)